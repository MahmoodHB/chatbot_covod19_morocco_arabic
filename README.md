# Chatbot for COVID-19

A simple website featuring a chatbot about advices about covid-19.

## Getting Started

### Prerequisites

The core of the chatbot is built using python 3.5.1 with keras library.

### Installing

You must install the libraries written in requirements.txt

You may first create a virtual environment for the project.
```
python3 -m venv venv
```

#### github :
get project from github with this commend.
```bash
git clone https://github.com/othmanAKKA/chatbot-covid19-darija-arabic.git
```

Before installing libraries.
Then, install required libraries:

```
pip install –r requirements.txt
```

### Training

After setting the paths correctly, run the file ```train.py``` from python IDLE, from CMD or google colab by running the following:

``` python train.py -ep 100 -bs 64 -n 256 ```

Note: Each of the command line arguments has a default value so you may remove it from the command if you want.

### Test

To run the website, right-click at ```server.py```  and choose Edit with IDLE, then run the script ```F5```.
Or, you may run it from cmd by running the following command at the same project directory:

```
python serveur.py
```


## Built With

* [python]( https://www.python.org/) - interpreted high-level programming language for general-purpose programming.
* [keras]( https://keras.io/) - a high-level neural networks API.

## TODO

* Weights file needs update
* Additional commenting needed
* Use web interface 
* create server with Flask
* Enhancing the keras model
