from argparse import ArgumentParser

from models import Model_Chat

parser = ArgumentParser()

# LSTM networks dimentionality or neurons numbers
parser.add_argument('-n', nargs=1, default=['256'])
# Tringing batch size
parser.add_argument('-bs', nargs=1, default=['64'])
# Number of epoches
parser.add_argument('-ep', nargs=1, default=['100'])

args = parser.parse_args()
n = int(args.n[0])
bs = int(args.bs[0])
ep = int(args.ep[0])

model = Model_Chat()

model.readData(['statistics.txt', 'infos.txt'],['synonyms.txt'])
model.fillTrainingMatrices()
model.createTrainingModels(neurons=n)
model.loadModelWeights('weights.h5')
model.trainModel(batchSize=bs, epochs=ep)
