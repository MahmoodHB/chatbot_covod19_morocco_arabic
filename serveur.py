import json

from flask import Flask, render_template, request

from models import Model_Chat


model = Model_Chat()

model.readData(['statistics.txt', 'infos.txt'])
model.createTrainingModels()
model.loadModelWeights('weights.h5')
model.creatInferenceModels()

app = Flask(__name__)


@app.route('/')
def output():
    return render_template('index.html')


@app.route('/web', methods=['POST'])
def worker():
    data = request.get_json()
    ans = model.predictAnswer(data)
    ans = ans.replace('_END', '')
    imgs = []
    if '$' in ans:
        ans = ans.split()
        ids = ans[-1].replace('$', '').split('-')
        for i in ids:
            imgs.append(int(i))
        ans = ' '.join(ans[:-1])
    ret = [ans, imgs]
    return json.dumps(ret)


if __name__ == '__main__':
    #app.run()
    app.run(host="127.0.0.1", port=5000, debug=True, use_reloader=False)